package db

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"time"

	"bitbucket.org/teampaysolut/glog_lib"
	"github.com/pkg/errors"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
)

type Logger struct {
	logger *glog_lib.Entry
}

func (l Logger) Log(_ context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	switch level {
	case pgx.LogLevelTrace, pgx.LogLevelDebug, pgx.LogLevelInfo:
		l.logger.Debug(msg, data)
	case pgx.LogLevelWarn:
		l.logger.Warn(msg, data)
	default:
		l.logger.Error(msg, data)
	}
}

func NewLogger(entry *glog_lib.Entry) Logger {
	return Logger{logger: entry}
}

func NewDB(entry *glog_lib.Entry) (*sqlx.DB, error) {
	var (
		host     = os.Getenv("DB_HOST")
		port     = os.Getenv("DB_PORT")
		user     = os.Getenv("DB_USER")
		password = os.Getenv("DB_PASSWORD")
		dbname   = os.Getenv("DB_NAME")
	)

	appName, ok := entry.Data["application"]
	if !ok {
		entry.Warn("Application name is not set to logger")
	}

	connString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable application_name=%s",
		host, port, user, password, dbname, appName)

	config, err := pgx.ParseConfig(connString)
	if err != nil {
		return nil, errors.Wrap(err, "can't parse db connString config")
	}
	config.LogLevel = pgx.LogLevelTrace
	config.Logger = NewLogger(entry)

	idle, err := strconv.Atoi(os.Getenv("DB_MAX_IDLE_CONNS"))
	if err != nil {
		return nil, errors.Wrap(err, "can't convert DB_MAX_IDLE_CONNS to int")
	}
	if idle == 0 {
		idle = 2
	}

	open, err := strconv.Atoi(os.Getenv("DB_MAX_OPEN_CONNS"))
	if err != nil {
		return nil, errors.Wrap(err, "can't convert DB_MAX_OPEN_CONNS to int")
	}
	if open == 0 {
		open = 2
	}

	db := stdlib.OpenDB(*config)
	ddx := sqlx.NewDb(db, "pgx")
	ddx.SetConnMaxLifetime(5 * time.Minute)
	ddx.SetMaxOpenConns(open)
	ddx.SetMaxIdleConns(idle)

	err = ddx.Ping()
	if err != nil {
		return nil, errors.Wrap(err, "can't ping db")
	}
	return ddx.Unsafe(), nil
}
