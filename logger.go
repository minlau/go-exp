package main

import (
	"os"
	"strings"

	"bitbucket.org/teampaysolut/glog_lib"
	"github.com/pkg/errors"
)

func CreateLogEntry(appName, logFilePath string) (*os.File, *glog_lib.Entry, error) {
	logFilePath = strings.TrimSuffix(logFilePath, string(os.PathSeparator))
	logFile, err := os.OpenFile(logFilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0o644)
	if err != nil {
		return nil, nil, errors.Wrap(err, "can't create/open log file")
	}

	hostname, err := os.Hostname()
	if err != nil {
		logFile.Close()
		return nil, nil, errors.Wrap(err, "can't get hostname")
	}

	logger := glog_lib.New(hostname)
	logger.SetOutput(logFile)

	appLogEntry := logger.NewEntry().WithApplication(appName)
	return logFile, appLogEntry, nil
}
