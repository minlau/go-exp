package server

import (
	"fmt"
	"go-exp/server/handler"

	"bitbucket.org/teampaysolut/glog_lib"
	"bitbucket.org/teampaysolut/gutils_lib/health"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type Server struct {
	logger *glog_lib.Entry
	db     *sqlx.DB
	engine *gin.Engine
}

func NewServer(db *sqlx.DB, logger *glog_lib.Entry, middlewares ...gin.HandlerFunc) Server {
	gin.SetMode(gin.ReleaseMode)
	engine := gin.New()
	engine.Use(gin.Logger(), recoveryMiddleware(logger))

	server := Server{
		logger: logger,
		db:     db,
		engine: engine,
	}
	server.initRoutes(middlewares...)
	return server
}

func (s Server) Run(port string) error {
	err := s.engine.Run(fmt.Sprintf(":%s", port))
	if err != nil {
		return errors.Wrap(err, "gin server has stopped")
	}
	return nil
}

func (s Server) initRoutes(middlewares ...gin.HandlerFunc) {
	s.engine.GET("/health", health.NewHealthCheckHandler(s.logger, s.db))

	s.engine.Use(middlewares...)
	s.engine.Use(requestLoggerMiddleware(s.logger))

	s.engine.POST("/produce-kafka-message", handler.ProduceKafkaMessage)
}
