package response

type ErrorResponse struct {
	Error ErrorRespType `json:"error"`
}

type ErrorRespType struct {
	Code    string            `json:"code"`
	Message string            `json:"message"`
	Data    map[string]string `json:"data,omitempty"`
}
