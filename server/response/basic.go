package response

import (
	"net/http"
	"strconv"

	apierrors "bitbucket.org/teampaysolut/gutils_lib/errors"
	"github.com/gin-gonic/gin"
)

func Response(context *gin.Context, statusCode int, data interface{}) {
	context.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	context.Writer.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
	context.Writer.Header().Set("Access-Control-Allow-Headers", "Authorization")
	context.JSON(statusCode, data)
}

func Success(context *gin.Context, data interface{}) {
	Response(context, http.StatusOK, data)
}

func Error(context *gin.Context, statusCode int, message string, data map[string]string) {
	Response(context, statusCode, ErrorResponse{
		Error: ErrorRespType{
			Code:    strconv.Itoa(statusCode),
			Message: message,
			Data:    data,
		},
	})
}

func ErrorFromApiError(context *gin.Context, statusCode int, apiError apierrors.Api, data map[string]string) {
	Response(context, statusCode, ErrorResponse{
		Error: ErrorRespType{
			Code:    apiError.Code(),
			Message: apiError.Error(),
			Data:    data,
		},
	})
}
