package handler

import (
	"go-exp/kafka_sender"
	"go-exp/server/response"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/segmentio/kafka-go"
)

func ProduceKafkaMessage(c *gin.Context) {
	logger := getLogger(c)

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		logger.Error("can't read from request body")
		response.Error(c, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}

	msg := kafka_sender.MsgWrapper{
		Message: kafka.Message{
			Value: body,
		},
		Logger: logger,
	}
	kafka_sender.KafkaSenderEngine.PushMessageToPipe(msg, kafka_sender.KafkaSenderEngine.TestCh)

	response.Success(c, nil)
}
