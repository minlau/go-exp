package handler

import (
	"bitbucket.org/teampaysolut/glog_lib"
	"github.com/gin-gonic/gin"
)

func getLogger(c *gin.Context) *glog_lib.Entry {
	logger, exists := c.Get("logger")
	if !exists {
		// Reason for panic is that logger should always exist. If logger does not exist then it is a bug in code.
		// Without bugfix there is no way to solve this issue.
		// In all cases when request logger does not exist handler must be "stopped" and 500 response is returned.
		// That's how it will work in case of panic.
		// Alternative solution is to redesign handlers and make request logger accessible directly in handler.
		panic("request logger not found in gin context")
	}
	return logger.(*glog_lib.Entry)
}
