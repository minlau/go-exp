package server

import (
	"go-exp/server/response"
	"net"
	"net/http"
	"os"
	"runtime/debug"
	"strconv"
	"strings"

	"bitbucket.org/teampaysolut/glog_lib"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func requestLoggerMiddleware(logEntry *glog_lib.Entry) gin.HandlerFunc {
	return func(c *gin.Context) {
		requestLogger := logEntry.FromHttpRequest(c.Request)

		if requestLogger.IsDebug() {
			requestLogger.LogHttpRequestBody(c.Request, logrus.DebugLevel)
		}

		requestLogger = requestLogger.WithType("processing")
		c.Set("logger", requestLogger)

		c.Next()
	}
}

// recoveryMiddleware returns a middleware that recovers from panics and logs it to logger.
func recoveryMiddleware(logEntry *glog_lib.Entry) gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				// Check for a broken connection, as it is not really a
				// condition that warrants a panic stack trace.
				var brokenPipe bool
				if ne, ok := err.(*net.OpError); ok {
					if se, ok := ne.Err.(*os.SyscallError); ok {
						if strings.Contains(strings.ToLower(se.Error()), "broken pipe") ||
							strings.Contains(strings.ToLower(se.Error()), "connection reset by peer") {
							brokenPipe = true
						}
					}
				}

				// Prints out panic message with full stack trace.
				logEntry.Errorf("handler interrupted due to panic: '%s'. stack: %s", err, debug.Stack())

				if brokenPipe {
					// If the connection is dead, we can't write a status to it.
					c.Error(err.(error)) // nolint: errcheck
					c.Abort()
				} else {
					errResp := response.ErrorResponse{
						Error: response.ErrorRespType{
							Code:    strconv.Itoa(http.StatusInternalServerError),
							Message: http.StatusText(http.StatusInternalServerError),
						},
					}
					c.AbortWithStatusJSON(http.StatusInternalServerError, errResp)
				}
			}
		}()
		c.Next()
	}
}
