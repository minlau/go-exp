package main

import (
	"fmt"
	"go-exp/db"
	"go-exp/kafka/handler"
	"go-exp/kafka_sender"
	"go-exp/server"
	"log"
	"net/http"
	"os"

	"bitbucket.org/teampaysolut/gutils_lib/kafka_io"
	"bitbucket.org/teampaysolut/gutils_lib/kafka_io/middleware"
	gin_metrics "bitbucket.org/teampaysolut/gutils_lib/metrics/gin"
	kafka_metrics "bitbucket.org/teampaysolut/gutils_lib/metrics/kafka"
	"bitbucket.org/teampaysolut/gutils_lib/worker_pool"
	"github.com/joho/godotenv"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const ApplicationName = "go-exp"

func main() {
	// load configuration
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("can't load .env file. err: %s", err)
	}

	// create new logger
	logFile, logEntry, err := CreateLogEntry(ApplicationName, os.Getenv("LOG_FILE_PATH"))
	if err != nil {
		log.Fatalf("can't create log entry. err: %s", err)
	}
	defer logFile.Close()

	// create new db
	database, err := db.NewDB(logEntry)
	if err != nil {
		logEntry.Fatalf("can't create new db: %s", err)
	}
	defer database.Close()

	// start metrics http server
	metricsPort := os.Getenv("METRICS_PORT")
	if metricsPort == "" {
		logEntry.Fatalf("METRICS_PORT is not provided in environment variables")
	}
	go func() {
		err := http.ListenAndServe(fmt.Sprintf(":%s", metricsPort), promhttp.Handler())
		if err != nil {
			logEntry.Fatalf("metrics http server has stopped: %s", err)
		}
	}()

	// start kafka producers
	kafka_sender.InitKafkaSender(logEntry)
	kafka_sender.KafkaSenderEngine.RunPipeWatcher()

	// start kafka consumers
	recoveryMiddleware := middleware.NewRecoveryMiddleware(logEntry)
	metricsMiddleware, err := kafka_metrics.NewDefaultMetricsMiddleware(ApplicationName)
	if err != nil {
		logEntry.Fatalf("new kafka default metrics recorder: %s", err)
	}

	genericMessageConsumer := kafka_io.NewConsumer(os.Getenv("GENERIC_MESSAGE_TOPIC"), os.Getenv("GENERIC_MESSAGE_TOPIC_GROUP"), nil, logEntry)
	genericMessageConsumer.AddMiddlewares(recoveryMiddleware, metricsMiddleware)
	genericMessageConsumer.Consume(
		func() worker_pool.Worker {
			return handler.NewGenericMessageHandler(database)
		},
		nil,
	)

	// start main http server
	ginMetricsMiddleware := gin_metrics.NewDefaultMetricsMiddleware(ApplicationName)
	apiServer := server.NewServer(database, logEntry, ginMetricsMiddleware)
	err = apiServer.Run(os.Getenv("SERVER_PORT"))
	if err != nil {
		logEntry.Fatalf("http server has stopped: %s", err)
	}
}
