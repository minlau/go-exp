# Go experimental project

The purpose of this project is to have project where new Go code improvements/features could be experimented/tested. It
is not an example Go project to use as a base microservice repository code.

### Rules

- master branch contains base project
- new features/improvements are experimented on new branch made from master
- when new feature/improvement is done and is ready to be used then it can be merged to master branch These rules might
  be changed in the future.

### Tips

- ./xlibs folder should be used to place new features/implementations