package kafka_sender

import (
	"fmt"
	"os"

	"bitbucket.org/teampaysolut/glog_lib"
	"bitbucket.org/teampaysolut/gutils_lib/kafka_io"

	"github.com/segmentio/kafka-go"
)

const MessageBuffer = 100000

var KafkaSenderEngine *KafkaSender

type MsgWrapper struct {
	Message kafka.Message
	Logger  *glog_lib.Entry
}

type KafkaSender struct {
	TestCh chan MsgWrapper
	Logger *glog_lib.Entry
}

func InitKafkaSender(l *glog_lib.Entry) {
	KafkaSenderEngine = &KafkaSender{
		Logger: l,
		TestCh: make(chan MsgWrapper, MessageBuffer),
	}
}

func (w *KafkaSender) CleanupChannels() {
	close(w.TestCh)
	w.TestCh = make(chan MsgWrapper, MessageBuffer)
}

func (w KafkaSender) PushMessageToPipe(msg MsgWrapper, ch chan MsgWrapper) {
	msg.Logger.Debug("Push message to pipe", string(msg.Message.Value))
	ch <- msg
}

func (w KafkaSender) StartChannelMessagesPipe(topic string, ch chan MsgWrapper) {
	writer := kafka_io.NewWriter(topic, w.Logger)
	w.Logger.Debug(fmt.Sprintf("start producer on topic %s", topic))
	for data := range ch {
		data.Logger.Info(fmt.Sprintf("writing a message %+v to a topic %s", string(data.Message.Value), topic), string(data.Message.Value))
		writer.SetLogger(data.Logger)
		err := writer.WriteMessage(&data.Message)
		if err != nil {
			w.Logger.Error("kafka message send error", err)
		}
	}
	_ = writer.Close()
}

func (w KafkaSender) RunPipeWatcher() {
	go w.StartChannelMessagesPipe(os.Getenv("GENERIC_MESSAGE_TOPIC"), w.TestCh)
}
