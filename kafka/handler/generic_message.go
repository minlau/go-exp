package handler

import (
	"bitbucket.org/teampaysolut/gutils_lib/kafka_io"
	"github.com/jmoiron/sqlx"
)

type GenericMessageHandler struct {
	Db *sqlx.DB
}

func NewGenericMessageHandler(db *sqlx.DB) GenericMessageHandler {
	return GenericMessageHandler{
		Db: db,
	}
}

func (h GenericMessageHandler) Stop() {}

func (h GenericMessageHandler) Do(body interface{}, _ int) {
	body, kafkaHeaders, logger := kafka_io.ParseMsgWrapper(body)
	logger.Infof("received kafka message. Message: %s, Headers: %s", string(body.([]byte)), kafkaHeaders)
}
